package binarycamp.spray.common

import binarycamp.spray.common.Errors._
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class ErrorSpec extends FlatSpec {
  "Error" should "return false on hasTag when tag list is empty" in {
    Error1.hasTag(Group) should be(false)
  }

  it should "return true on hasGroup when group is defined" in {
    Error2.hasTag(Group) should be(true)
  }
}
