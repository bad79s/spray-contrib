package binarycamp.spray.pimp.json

import spray.json._

final class PimpedJsObject(val jsObject: JsObject) extends AnyVal {
  def apply(fieldName: String): Option[JsValue] = field(fieldName)

  def field(fieldName: String): Option[JsValue] = jsObject.fields.get(fieldName)

  def merge(that: JsObject): JsObject = JsObject(jsObject.fields ++ that.fields)

  def merge(field: JsField): JsObject = JsObject(jsObject.fields + field)

  def merge(fields: List[JsField]): JsObject = JsObject(jsObject.fields ++ fields)
}
