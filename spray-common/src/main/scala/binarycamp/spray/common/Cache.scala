package binarycamp.spray.common

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap
import scala.util.{ Success, Try }

final class Cache[T](initialCapacity: Int, maximumCapacity: Int) {
  private val cache: ConcurrentLinkedHashMap[String, T] = new ConcurrentLinkedHashMap.Builder[String, T]()
    .initialCapacity(initialCapacity).maximumWeightedCapacity(maximumCapacity).build()

  def apply(key: String)(genValue: ⇒ Try[T]): Try[T] =
    cache.get(key) match {
      case null ⇒ genValue match {
        case Success(value) ⇒
          val prior = cache.putIfAbsent(key, value)
          if (prior != null) Success(prior) else Success(value)
        case error ⇒ error
      }
      case value ⇒ Success(value)
    }

  def get(key: String): Option[T] = if (cache.containsKey(key)) Some(cache.get(key)) else None
}

object Cache {
  def apply[T](initialCapacity: Int = 10, maxCapacity: Int = 100): Cache[T] = new Cache[T](initialCapacity, maxCapacity)
}
