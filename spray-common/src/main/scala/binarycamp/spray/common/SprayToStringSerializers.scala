package binarycamp.spray.common

import spray.http.Uri

trait SprayToStringSerializers {
  implicit object UriToStringSerializer extends ToStringSerializer[Uri] {
    override def apply(uri: Uri): String = uri.toString
  }
}
