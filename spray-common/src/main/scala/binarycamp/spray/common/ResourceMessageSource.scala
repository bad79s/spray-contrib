package binarycamp.spray.common

import akka.actor.ActorRefFactory
import binarycamp.spray.common.MessageResolutionErrors.{ MessageNotFound, ResolutionFailed }
import java.text.MessageFormat
import java.util.Locale
import scala.annotation.tailrec
import scala.util.{ Failure, Success, Try }
import spray.util._

final class ResourceMessageSource(val baseName: String)(implicit refFactory: ActorRefFactory) extends MessageSource {
  private val cache = Cache[Properties]()

  override def apply(code: String, args: Seq[Any], locales: Seq[Locale]): ResolvedMessage = {
    @tailrec
    def resolve(remainder: Seq[Locale]): ResolvedMessage = {
      if (remainder.isEmpty) {
        findMessage(code, "") match {
          case Success(None)          ⇒ Left(MessageNotFound(code, locales))
          case Success(Some(message)) ⇒ Right(formatMessage(message, args))
          case Failure(exception)     ⇒ Left(ResolutionFailed(code, exception))
        }
      } else {
        val locale = remainder.head
        findMessage(code, suffix(locale)) match {
          case Success(None)          ⇒ resolve(remainder.tail)
          case Success(Some(message)) ⇒ Right(formatMessage(message, args, locale))
          case Failure(exception)     ⇒ Left(ResolutionFailed(code, exception))
        }
      }
    }

    resolve(locales)
  }

  private def findMessage(code: String, suffix: String): Try[Option[String]] = {
    val resourceName = s"$baseName$suffix.properties"
    cache(resourceName)(loadProperties(resourceName)).map(properties ⇒ properties.get(code))
  }

  private def loadProperties(name: String): Try[Properties] =
    actorSystem.dynamicAccess.classLoader.getResourceAsStream(name) match {
      case null ⇒ Success(Map.empty)
      case is ⇒
        val parser = new PropertiesParser(io.Source.fromInputStream(is).mkString)
        parser.Properties.run()
    }

  private def suffix(locale: Locale): String = {
    val builder = new StringBuilder()
    if (locale.getLanguage() != "") builder.append("_").append(locale.getLanguage())
    if (locale.getCountry() != "") builder.append("_").append(locale.getCountry())
    if (locale.getVariant() != "") builder.append("_").append(locale.getVariant())
    builder.toString()
  }

  private def formatMessage(message: String, args: Seq[Any]) =
    new MessageFormat(message).format(args.toArray)

  private def formatMessage(message: String, args: Seq[Any], locale: Locale) =
    new MessageFormat(message, locale).format(args.toArray)
}
