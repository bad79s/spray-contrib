package binarycamp.spray.common

import spray.http.LanguageRange

trait ErrorTranslator[T] {
  def apply(error: Error, languages: Seq[LanguageRange] = Nil): T
}
