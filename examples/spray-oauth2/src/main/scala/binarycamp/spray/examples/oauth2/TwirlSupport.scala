package binarycamp.spray.examples.oauth2

import play.twirl.api.{ Html, Txt, Xml }
import spray.http.MediaTypes._
import spray.http._
import spray.httpx.marshalling.Marshaller

trait TwirlSupport {

  implicit val twirlHtmlMarshaller =
    twirlMarshaller[Html](`text/html`, `application/xhtml+xml`)

  implicit val twirlTxtMarshaller =
    twirlMarshaller[Txt](ContentTypes.`text/plain`)

  implicit val twirlXmlMarshaller =
    twirlMarshaller[Xml](`text/xml`)

  protected def twirlMarshaller[T](marshalTo: ContentType*): Marshaller[T] =
    Marshaller.delegate[T, String](marshalTo: _*)(_.toString)
}

object TwirlSupport extends TwirlSupport
