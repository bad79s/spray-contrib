package binarycamp.spray.examples.oauth2

import akka.actor.ActorSystem
import binarycamp.spray.data.MarshallableError
import binarycamp.spray.examples.oauth2.html.ApprovalPage
import binarycamp.spray.login._
import binarycamp.spray.oauth2.authentication._
import binarycamp.spray.oauth2.server.TokenService.TokenRevocationFailed
import binarycamp.spray.oauth2.server._
import binarycamp.spray.oauth2.server.flows._
import spray.http.Uri.Path
import spray.http.{ StatusCodes, Uri }
import spray.httpx.SprayJsonSupport
import spray.routing.Directives._
import spray.routing._

object Main extends App with SimpleRoutingApp with InfoSprayJsonFormats with SprayJsonSupport with TwirlSupport {
  implicit val system = ActorSystem("example")

  implicit val executor = system.dispatcher

  val userService = Users.userService
  val clientToUserMapper = Users.clientToUserMapper
  val clientService = ClientService.default
  val tokenService = InMemoryTokenService.default

  val handlers = ImplicitFlow.handlers(clientService, tokenService) ++
    ClientCredentialsFlow.handlers(clientService, clientToUserMapper, tokenService) ++
    UserPasswordCredentialsFlow.handlers(clientService, userService, tokenService)

  val loginPage = new LoginPageRoute {
    override def apply(error: Option[String]): Route = getFromResource("login.html")
  }

  val approvalPage = new ApprovalPageRoute[User] {
    override def apply(user: UserInfo[User], client: Client, scopes: Set[Scope], approvalEndpoint: Path, denialEndpoint: Path): Route =
      complete(ApprovalPage(user, client, scopes, approvalEndpoint, denialEndpoint))
  }

  val errorPage = new ErrorPageRoute {
    override def apply(error: MarshallableError): Route = getFromResource("error.html")
  }

  val loginService = LoginService(userService, loginPage)

  val authenticator = BearerTokenAuthenticator(DefaultAccessTokenAuthenticator(tokenService, userService))

  val revocationHandler = new RevocationHandler {
    override def revoke = {
      loginService.logout().hflatMap { _ ⇒
        extract(ctx ⇒ authenticator.extractAccessToken(ctx)).flatMap {
          case Right(accessToken) ⇒ onSuccess(tokenService.revokeToken(accessToken))
          case Left(_)            ⇒ provide(TokenRevocationFailed("Couldn't extract token!"))
        }
      }
    }
  }

  val oauth2Server = AuthorizationServer(loginService, clientService.authenticator, handlers, revocationHandler, Scopes.default, approvalPage, errorPage)

  startServer("localhost", 8080) {
    (pathEndOrSingleSlash | path("index.html")) {
      redirect(Uri("/app"), StatusCodes.Found)
    } ~ pathPrefix("app") {
      getFromResource("index.html")
    } ~ path("api") {
      authenticate(authenticator) { restrictedUser ⇒
        get {
          complete(SecretInfo("secret stuff", restrictedUser.user))
        }
      }
    } ~ loginService.route ~ oauth2Server.route
  }

  sys.addShutdownHook(system.shutdown())
}
