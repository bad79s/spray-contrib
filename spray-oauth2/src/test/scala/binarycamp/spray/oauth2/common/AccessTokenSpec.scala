package binarycamp.spray.oauth2.common

import org.scalatest.FlatSpec

class AccessTokenSpec extends FlatSpec {
  "AccessToken" should "produce IllegalArgumentException when an additional attribute has a reserved key" in {
    for (key ← AccessToken.ReservedKeys) {
      intercept[IllegalArgumentException] {
        AccessToken("tokenType", "token", Some(Map(key -> "value")))
      }
    }
  }
}
