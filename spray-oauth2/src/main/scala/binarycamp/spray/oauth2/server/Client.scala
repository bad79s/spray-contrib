package binarycamp.spray.oauth2.server

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.ScopePolicies._
import scala.util.control.NonFatal
import spray.http.Uri
import spray.routing.Rejection

case class Client(
    id: String,
    secret: String,
    name: String,
    grantTypes: Set[GrantType],
    scopes: Set[String],
    defaultScopes: Option[Set[String]],
    scopePolicy: ScopePolicy,
    redirectionUris: Seq[Uri],
    needsApproval: Boolean) {

  require(defaultScopes.isEmpty || defaultScopes.get.subsetOf(scopes), "Default scopes must be a subset of all scopes")
  redirectionUris.foreach { uri ⇒
    require(isValid(uri), s"Redirection URI ${uri.toString()} must be an absolute URI with no fragment component")
  }

  def hasAuthorizationCode: Boolean = grantTypes.contains(GrantTypes.AuthorizationCode)

  def hasClientCredentials: Boolean = grantTypes.contains(GrantTypes.ClientCredentials)

  def hasPassword: Boolean = grantTypes.contains(GrantTypes.Password)

  def hasImplicit: Boolean = grantTypes.contains(GrantTypes.Implicit)

  def hasRefreshToken: Boolean = grantTypes.contains(GrantTypes.RefreshToken)

  def isConfidential: Boolean = hasAuthorizationCode || hasPassword

  def isValid(uri: Uri): Boolean = uri.isAbsolute && uri.fragment.isEmpty

  private val registeredUris = redirectionUris.map(_.toString())

  def validateRedirectionUri(redirectionUri: Option[String]): Either[Rejection, Uri] =
    redirectionUri match {
      case Some(uri) ⇒ validateRedirectionUri(uri)
      case None      ⇒ defaultRedirectionUri
    }

  private def validateRedirectionUri(redirectionUri: String): Either[Rejection, Uri] =
    (for {
      uri ← try { Some(Uri(redirectionUri)) } catch { case NonFatal(e) ⇒ None }
      if isValid(uri)
      if isConfidential && redirectionUris.isEmpty | registeredUris.exists(redirectionUri.startsWith)
    } yield uri).toRight(ErrorRejection(InvalidRedirectEndpoint))

  private def defaultRedirectionUri: Either[Rejection, Uri] =
    if (redirectionUris.size == 1) Right(redirectionUris.head) else Left(ErrorRejection(InvalidRedirectEndpoint))

  def validateScopes(ids: Option[Set[String]]): Either[Rejection, Set[String]] = scopePolicy match {
    case FailOnInvalid    ⇒ validateScopes(ids.orElse(defaultScopes).getOrElse(Set()))
    case DropInvalid      ⇒ validateScopes(ids.map(_.intersect(scopes)).filter(!_.isEmpty).orElse(defaultScopes).getOrElse(Set()))
    case AlwaysUseDefault ⇒ validateScopes(defaultScopes.getOrElse(Set()))
  }

  private def validateScopes(ids: Set[String]): Either[Rejection, Set[String]] =
    if (ids.isEmpty) Left(ErrorRejection(EmptyScope))
    else {
      val unknown = ids.diff(scopes)
      if (unknown.isEmpty) Right(ids) else Left(ErrorRejection(UnsupportedScope(unknown.toSet)))
    }
}

sealed trait GrantType

object GrantTypes {
  case object AuthorizationCode extends GrantType {
    override def toString: String = "authorization_code"
  }
  case object RefreshToken extends GrantType {
    override def toString: String = "refresh_token"
  }
  case object ClientCredentials extends GrantType {
    override def toString: String = "client_credentials"
  }
  case object Password extends GrantType {
    override def toString: String = "password"
  }
  case object Implicit extends GrantType {
    override def toString: String = "implicit"
  }
  case class Additional(grantName: String) extends GrantType {
    override def toString: String = grantName
  }
  def parse(value: String): GrantType = value.toLowerCase match {
    case grantType if grantType == AuthorizationCode.toString ⇒ AuthorizationCode
    case grantType if grantType == RefreshToken.toString ⇒ RefreshToken
    case grantType if grantType == ClientCredentials.toString ⇒ ClientCredentials
    case grantType if grantType == Password.toString ⇒ Password
    case grantType if grantType == Implicit.toString ⇒ Implicit
    case _ ⇒ Additional(value)
  }
}

class IllegalGrantTypeException(value: String) extends RuntimeException(s"Illegal GrantType $value")

class IllegalClientTypeException(value: String) extends RuntimeException(s"Illegal ClientType $value")

sealed trait ScopePolicy

object ScopePolicies {
  case object FailOnInvalid extends ScopePolicy
  case object DropInvalid extends ScopePolicy
  case object AlwaysUseDefault extends ScopePolicy

  def parse(value: String): ScopePolicy = value.toLowerCase match {
    case "fail-on-invalid"    ⇒ FailOnInvalid
    case "drop-invalid"       ⇒ DropInvalid
    case "always-use-default" ⇒ AlwaysUseDefault
    case _                    ⇒ throw new IllegalScopePolicyException(value)
  }
}

class IllegalScopePolicyException(value: String) extends RuntimeException(s"Illegal ScopePolicy $value")
