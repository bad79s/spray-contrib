package binarycamp.spray.oauth2.server

import binarycamp.spray.common._
import binarycamp.spray.data.MarshallableError
import binarycamp.spray.login.LoginDirectives._
import binarycamp.spray.login.LoginService
import binarycamp.spray.oauth2.server.ErrorSprayJsonSupport._
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.RequestResponseElements._
import binarycamp.spray.oauth2.server.TokenService.{ TokenRevocationFailed, TokenRevoked }
import binarycamp.spray.pimp._
import binarycamp.spray.session.CookieBaker
import binarycamp.spray.session.PicklingSupport._
import binarycamp.spray.session.SessionDirectives._
import spray.json.{ JsString, JsObject }
import scala.concurrent.ExecutionContext
import scala.language.existentials
import scala.pickling._
import scala.pickling.binary._
import scala.util.control.NonFatal
import spray.http.Uri.Path
import spray.http._
import spray.httpx.unmarshalling._
import spray.routing.AuthenticationFailedRejection.CredentialsMissing
import spray.routing.Directives._
import spray.routing._
import spray.util._

sealed trait AuthorizationServer[U] {
  def loginService: LoginService[U]

  def handlers: Seq[RequestHandler]

  def withHandlers(handlers: RequestHandler*): AuthorizationServer[U]

  def revocationHandler: RevocationHandler

  def withRevocationHandler(handler: RevocationHandler): AuthorizationServer[U]

  def clientAuthenticator: Option[ClientAuthenticator]

  def withClientAuthenticator(authenticator: ClientAuthenticator): AuthorizationServer[U]

  def scopes: Set[Scope]

  def withScopes(scopes: Scope*): AuthorizationServer[U]

  def authorizationEndpoint: Path

  def withAuthorizationEndpoint(path: Path): AuthorizationServer[U]

  def approvalEndpoint: Path

  def withApprovalEndpoint(path: Path): AuthorizationServer[U]

  def denialEndpoint: Path

  def withDenialEndpoint(path: Path): AuthorizationServer[U]

  def revocationEndpoint: Path

  def withRevocationEndpoint(path: Path): AuthorizationServer[U]

  def tokenEndpoint: Path

  def withTokenEndpoint(path: Path): AuthorizationServer[U]

  def approvalPage: ApprovalPageRoute[U]

  def withApprovalPage(route: ApprovalPageRoute[U]): AuthorizationServer[U]

  def errorPage: ErrorPageRoute

  def withErrorPage(route: ErrorPageRoute): AuthorizationServer[U]

  def route: Route
}

object AuthorizationServer {
  def apply[U](loginService: LoginService[U], handlers: Seq[RequestHandler], revocationHandler: RevocationHandler, scopes: Set[Scope],
               approvalPage: ApprovalPageRoute[U], errorPage: ErrorPageRoute)(implicit settings: ServerSettings, baker: CookieBaker,
                                                                              ec: ExecutionContext, logger: LoggingContext): AuthorizationServer[U] =
    AuthorizationServerImpl[U](loginService, handlers, revocationHandler, None, scopes,
      settings.authorizationEndpoint, settings.approvalEndpoint, settings.denialEndpoint,
      settings.revocationEndpoint, settings.tokenEndpoint, approvalPage, errorPage)

  def apply[U](loginService: LoginService[U], clientAuthenticator: ClientAuthenticator,
               handlers: Seq[RequestHandler], revocationHandler: RevocationHandler, scopes: Set[Scope],
               approvalPage: ApprovalPageRoute[U], errorPage: ErrorPageRoute)(implicit settings: ServerSettings, baker: CookieBaker,
                                                                              ec: ExecutionContext, logger: LoggingContext): AuthorizationServer[U] =
    AuthorizationServerImpl[U](loginService, handlers, revocationHandler, Some(clientAuthenticator), scopes,
      settings.authorizationEndpoint, settings.approvalEndpoint, settings.denialEndpoint,
      settings.revocationEndpoint, settings.tokenEndpoint, approvalPage, errorPage)
}

// format: OFF
private case class AuthorizationServerImpl[U](loginService: LoginService[U], handlers: Seq[RequestHandler], revocationHandler: RevocationHandler,
                                              clientAuthenticator: Option[ClientAuthenticator], scopes: Set[Scope],
                                              authorizationEndpoint: Path, approvalEndpoint: Path,
                                              denialEndpoint: Path, revocationEndpoint: Path, tokenEndpoint: Path,
                                              approvalPage: ApprovalPageRoute[U], errorPage: ErrorPageRoute)
                                             (implicit val baker: CookieBaker, val ec: ExecutionContext,
                                              val logger: LoggingContext)
    // format: ON
    extends AuthorizationServer[U] with AuthorizationEndpoint[U] with TokenEndpoint[U] with Helpers with ErrorHandlers {

  val knownScopeIds = scopes.map(_.id)

  override def route: Route = Seq(authorizationEndpointRoute, tokenEndpointRoute).partition(_.isDefined) match {
    case (Nil, _)          ⇒ reject
    case (route :: Nil, _) ⇒ route.get
    case (routes, _)       ⇒ routes.map(_.get).reduce(_ ~ _)
  }

  override def withHandlers(handlers: RequestHandler*): AuthorizationServer[U] = copy(handlers = handlers)
  override def withRevocationHandler(handler: RevocationHandler): AuthorizationServer[U] = copy(revocationHandler = handler)
  override def withClientAuthenticator(authenticator: ClientAuthenticator): AuthorizationServer[U] =
    copy(clientAuthenticator = Some(authenticator))
  override def withScopes(scopes: Scope*): AuthorizationServer[U] = copy(scopes = scopes.toSet)
  override def withAuthorizationEndpoint(path: Path): AuthorizationServer[U] = copy(authorizationEndpoint = path)
  override def withApprovalEndpoint(path: Path): AuthorizationServer[U] = copy(approvalEndpoint = path)
  override def withRevocationEndpoint(path: Path): AuthorizationServer[U] = copy(revocationEndpoint = path)
  override def withDenialEndpoint(path: Path): AuthorizationServer[U] = copy(denialEndpoint = path)
  override def withTokenEndpoint(path: Path): AuthorizationServer[U] = copy(tokenEndpoint = path)
  override def withApprovalPage(route: ApprovalPageRoute[U]): AuthorizationServer[U] = copy(approvalPage = route)
  override def withErrorPage(route: ErrorPageRoute): AuthorizationServer[U] = copy(errorPage = route)
}

private trait AuthorizationEndpoint[U] {
  this: AuthorizationServerImpl[U] ⇒

  type ARH = AuthorizationRequestHandler

  val authorizationRequestHandlers = handlers.filterByType[ARH]

  def authorizationEndpointRoute: Option[Route] =
    if (authorizationRequestHandlers.isEmpty) None
    else Some(path(authorizationEndpoint.asMatcher) {
      get {
        handleAuthorizationRequestErrors() {
          login(loginService) { userInfo ⇒
            queryToRequestParams { params ⇒
              authorizationRequestHandler(params) { handler ⇒
                handler.clientContext(params) { (client, redirectionUri) ⇒
                  handleAuthorizationRequestErrors(handler, redirectionUri, params) {
                    validateAuthorizationRequest(handler, params) { params ⇒
                      scopeIds(client, params) { scopeIds ⇒
                        if (client.needsApproval) {
                          setSession("arparams" pickle params, "arscopes" pickle scopeIds) {
                            approvalPage(userInfo, client, filterScopes(scopeIds), approvalEndpoint, denialEndpoint)
                          }
                        } else handler.handle(userInfo.id, client, redirectionUri, scopeIds, params)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } ~ path(approvalEndpoint.asMatcher) {
      post {
        handleAuthorizationRequestErrors() {
          authenticate(loginService) { userInfo ⇒
            session("arparams".unpickleAs[RequestParameters], "arscopes".unpickleAs[Set[String]]) { (params, scopeIds) ⇒
              authorizationRequestHandler(params) { handler ⇒
                handler.clientContext(params) { (client, redirectionUri) ⇒
                  handleAuthorizationRequestErrors(handler, redirectionUri, params) {
                    setSession("arparams".remove, "arscopes".remove) {
                      handler.handle(userInfo.id, client, redirectionUri, scopeIds, params)
                    }
                  }
                }
              }
            }
          }
        }
      }
    } ~ path(denialEndpoint.asMatcher) {
      post {
        handleAuthorizationRequestErrors() {
          authenticate(loginService) { user ⇒
            session("arparams".unpickleAs[RequestParameters]) { params ⇒
              authorizationRequestHandler(params) { handler ⇒
                handler.clientContext(params) { (client, redirectionUri) ⇒
                  handleAuthorizationRequestErrors(handler, redirectionUri, params) {
                    setSession("arparams".remove, "arscopes".remove) {
                      reject(ErrorRejection(UserHasDeniedAccess))
                    }
                  }
                }
              }
            }
          }
        }
      }
    } ~ path(revocationEndpoint.asMatcher) {
      post {
        revocationHandler.revoke {
          case TokenRevoked             ⇒ complete("ok")
          case TokenRevocationFailed(_) ⇒ complete(StatusCodes.BadRequest)
        }
      }
    })

  def handleAuthorizationRequestErrors(): Directive0 = handleErrors(error ⇒ errorPage(error))

  def authorizationRequestHandler(params: RequestParameters): Directive1[ARH] =
    if (!params.contains(ResponseType)) reject(ErrorRejection(MissingRequiredParameter(ResponseType)))
    else if (params.isDuplicate(ResponseType)) reject(ErrorRejection(DuplicateParameter(Set(ResponseType))))
    else authorizationRequestHandlers.find(_.canHandle(params)) match {
      case Some(handler) ⇒ provide(handler)
      case None          ⇒ reject(ErrorRejection(UnsupportedResponseType(params(ResponseType))))
    }

  def handleAuthorizationRequestErrors(handler: ARH, redirectionUri: Uri, params: RequestParameters): Directive0 =
    handleErrors(error ⇒ handler.handleError(error, redirectionUri, params))

  def validateAuthorizationRequest(handler: ARH, params: RequestParameters): Directive1[RequestParameters] =
    requireNoFragment.hflatMap { _ ⇒
      if (params.hasDuplicateParameters) reject(ErrorRejection(DuplicateParameter(params.duplicateParameters)))
      else handler.validate(params)
    }

  def filterScopes(scopeIds: Set[String]): Set[Scope] = scopes.filter(scope ⇒ scopeIds.contains(scope.id))
}

private trait TokenEndpoint[U] {
  this: AuthorizationServerImpl[U] ⇒

  type TRH = TokenRequestHandler
  type RRH = RefreshRequestHandler
  type ATRH = AccessTokenRequestHandler

  val refreshTokenRequestHandler = handlers.findByType[RRH]
  val accessTokenRequestHandlers = handlers.filterByType[ATRH]

  def tokenEndpointRoute: Option[Route] =
    if (accessTokenRequestHandlers.isEmpty) None
    else Some(path(tokenEndpoint.asMatcher) {
      post {
        handleTokenRequestErrors {
          (formToRequestParams | jsonToRequestParams) { params ⇒
            validateTokenRequest(params) {
              if (isRefreshRequest(params)) refreshRequestRoute(params)
              else accessTokenRequestRoute(params)
            }
          }
        }
      }
    })

  def handleTokenRequestErrors: Directive0 = handleErrors(error ⇒ complete(error))

  def validateTokenRequest(params: RequestParameters): Directive0 =
    requireNoFragment.hflatMap { _ ⇒
      if (params.hasDuplicateParameters) reject(ErrorRejection(DuplicateParameter(params.duplicateParameters)))
      else if (params.contains(ClientSecret)) reject(ErrorRejection(ClientCredentialsInReqBody))
      else pass
    }

  def isRefreshRequest(params: RequestParameters): Boolean = params.get(GrantType).exists(_ == "refresh_token")

  def refreshRequestRoute(params: RequestParameters): Route =
    if (refreshTokenRequestHandler.isEmpty) reject(ErrorRejection(UnsupportedGrantType(params.get(GrantType))))
    else {
      val handler = refreshTokenRequestHandler.get
      tryAuthenticate { client ⇒
        validateAuthenticatedClient(client, handler, params) {
          handler.handle(client, parseScopeIds(params), params)
        }
      } {
        clientFromParams(handler, params) { client ⇒
          handler.handle(client, parseScopeIds(params), params)
        }
      }
    }

  def accessTokenRequestRoute(params: RequestParameters): Route =
    accessTokenRequestHandler(params) { handler ⇒
      tryAuthenticate { client ⇒
        validateAuthenticatedClient(client, handler, params) {
          scopeIds(client, params) { scopeIds ⇒
            handler.handle(client, scopeIds, params)
          }
        }
      } {
        clientFromParams(handler, params) { client ⇒
          scopeIds(client, params) { scopeIds ⇒
            handler.handle(client, scopeIds, params)
          }
        }
      }
    }

  def accessTokenRequestHandler(params: RequestParameters): Directive1[ATRH] =
    accessTokenRequestHandlers.find(_.canHandle(params)) match {
      case Some(handler) ⇒ provide(handler)
      case None          ⇒ reject(ErrorRejection(UnsupportedGrantType(params.get(GrantType))))
    }

  def tryAuthenticate(primary: Client ⇒ Route)(alternative: ⇒ Route): Route =
    if (clientAuthenticator.isEmpty) alternative
    else authenticate(clientAuthenticator.get).recoverPF({
      case AuthenticationFailedRejection(CredentialsMissing, _) :: _ ⇒ Route.toDirective(alternative): Directive1[Client]
    })(primary)

  def validateAuthenticatedClient(client: Client, handler: TRH, params: RequestParameters): Directive0 =
    handler.optionalClient(params).flatMap {
      case Some(c) if c.id == client.id ⇒ pass
      case Some(c)                      ⇒ reject(ErrorRejection(ClientMustAuthenticate))
      case None                         ⇒ pass
    }

  def clientFromParams(handler: TRH, params: RequestParameters): Directive1[Client] =
    handler.optionalClient(params).flatMap {
      case Some(client) if params(GrantType) == GrantTypes.ClientCredentials.toString ⇒ reject(ErrorRejection(ClientMustAuthenticate))
      case Some(client) ⇒ provide(client)
      case None ⇒ reject(ErrorRejection(UnspecifiedClient))
    }
}

private trait Helpers {
  this: AuthorizationServerImpl[_] ⇒

  def queryToRequestParams: Directive1[RequestParameters] =
    extract(ctx ⇒ RequestParameters(ctx.request.uri.query.toMultiMap))

  def formToRequestParams: Directive1[RequestParameters] =
    entity(as[FormData]).flatMap {
      case FormData(fields) ⇒ provide(RequestParameters(fields))
      case _                ⇒ reject(ErrorRejection(InvalidFormData))
    }

  def jsonToRequestParams: Directive1[RequestParameters] = {
    entity(as[JsObject]).flatMap {
      case jsObject: JsObject ⇒ {
        val params = jsObject.fields.foldLeft(Seq.empty[(String, String)])((left, right) ⇒ right match {
          case (name, JsString(value)) ⇒ left ++ Seq((name, value))
          case _                       ⇒ left
        })
        provide(RequestParameters(params))
      }
      case _ ⇒ reject(ErrorRejection(InvalidFormData))
    }
  }

  def requireNoFragment: Directive0 =
    extract(_.request.uri.fragment).require(_.isEmpty, ErrorRejection(FragmentNotAllowed))

  def scopeIds(client: Client, params: RequestParameters): Directive1[Set[String]] =
    client.validateScopes(parseScopeIds(params)) match {
      case Right(ids) if ids.diff(knownScopeIds).isEmpty ⇒ provide(ids)
      case Right(ids)                                    ⇒ reject(ErrorRejection(UnsupportedScope(ids.diff(knownScopeIds))))
      case Left(rejection)                               ⇒ reject(rejection)
    }

  def parseScopeIds(params: RequestParameters): Option[Set[String]] = params.get(ScopeParam).map(_.split(" ").toSet)
}

private trait ErrorHandlers {
  def handleErrors(handler: MarshallableError ⇒ Route): Directive0 =
    handleExceptions(ExceptionHandler {
      case NonFatal(e) ⇒ handler(translator(ServerError))
    }) & handleRejections(RejectionHandler {
      case ErrorRejection(error) :: _ ⇒ handler(translator(error))
      case _                          ⇒ handler(translator(ServerError))
    })

  val errorMapper = ErrorToStatusCodeMapper {
    case _ ⇒ StatusCodes.BadRequest
  }

  val translator = new ErrorTranslator[MarshallableError] {
    override def apply(error: Error, languages: Seq[LanguageRange] = Nil): MarshallableError = {
      MarshallableError(error.code, "", None, None)
    }
  }
}
