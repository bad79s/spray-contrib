package binarycamp.spray.oauth2.server.flows

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.login.{ UserInfo, UserService }
import binarycamp.spray.oauth2.common.TokenSprayJsonSupport
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.RequestResponseElements._
import binarycamp.spray.oauth2.server._
import scala.concurrent.{ ExecutionContext, Future }
import spray.routing.Directives._
import spray.routing._

object UserPasswordCredentialsFlow {
  def handlers[U](clientService: ClientService, userService: UserService[U],
                  tokenService: TokenService)(implicit ec: ExecutionContext): Seq[RequestHandler] =
    Seq(new UserPasswordCredentialsHandler(clientService, userService, tokenService))
}

class UserPasswordCredentialsHandler[U](clientService: ClientService, userService: UserService[U],
                                        tokenService: TokenService)(implicit ec: ExecutionContext)
    extends AccessTokenRequestHandler with TokenSprayJsonSupport with RequestHandlerSupport {

  override def canHandle(params: RequestParameters): Boolean = params.get(GrantType).exists(_ == "password")

  override def optionalClient(params: RequestParameters): Directive1[Option[Client]] =
    optionalClient(clientService, params)

  override def handle(client: Client, scopes: Set[String], params: RequestParameters): Route = {
    if (!client.hasPassword) return reject(ErrorRejection(ClientMustAuthenticate))
    val missing = params.filterMissing(Username, Password)
    if (!missing.isEmpty) reject(ErrorRejection(UserMustAuthenticate))
    else {
      val f = userService.authenticate(params(Username), params(Password)).flatMap {
        case Some(UserInfo(id, _)) ⇒ tokenService.issueToken(client.id, id, GrantTypes.Password, scopes, true)
        case None                  ⇒ Future.successful(None)
      }
      onSuccess(f) {
        case Some(token) ⇒ complete(token)
        case None        ⇒ reject(ErrorRejection(TokenCreationFailed))
      }
    }
  }
}
