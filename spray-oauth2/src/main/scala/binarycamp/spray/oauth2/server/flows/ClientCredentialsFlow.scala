package binarycamp.spray.oauth2.server.flows

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.login.UserInfo
import binarycamp.spray.oauth2.common.TokenSprayJsonSupport
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.RequestResponseElements._
import binarycamp.spray.oauth2.server._
import scala.concurrent.{ ExecutionContext, Future }
import spray.routing.Directives._
import spray.routing._

object ClientCredentialsFlow {
  def handlers[U](clientService: ClientService, mapper: ClientToUserMapper[U],
                  tokenService: TokenService)(implicit ec: ExecutionContext): Seq[RequestHandler] =
    Seq(new ClientCredentialsRequestHandler(clientService, mapper, tokenService))
}

class ClientCredentialsRequestHandler[U](clientService: ClientService, mapper: ClientToUserMapper[U],
                                         tokenService: TokenService)(implicit ec: ExecutionContext)
    extends AccessTokenRequestHandler with TokenSprayJsonSupport with RequestHandlerSupport {

  override def canHandle(params: RequestParameters): Boolean = params.get(GrantType).exists(_ == "client_credentials")

  override def optionalClient(params: RequestParameters): Directive1[Option[Client]] =
    optionalClient(clientService, params)

  override def handle(client: Client, scopes: Set[String], params: RequestParameters): Route = {
    if (!client.hasClientCredentials) return reject(ErrorRejection(ClientMustAuthenticate))
    val f = mapper(client).flatMap {
      case Some(UserInfo(id, _)) ⇒ tokenService.issueToken(client.id, id, GrantTypes.ClientCredentials, scopes, false)
      case None                  ⇒ Future.successful(None)
    }
    onSuccess(f) {
      case Some(token) ⇒ complete(token)
      case None        ⇒ reject(ErrorRejection(TokenCreationFailed))
    }
  }
}

trait ClientToUserMapper[U] extends (Client ⇒ Future[Option[UserInfo[U]]])
