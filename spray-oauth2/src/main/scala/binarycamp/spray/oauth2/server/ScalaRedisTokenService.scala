package binarycamp.spray.oauth2.server

import akka.actor.ActorRefFactory
import akka.util.Timeout
import binarycamp.spray.oauth2.common.{ AccessToken, TokenResponse }
import binarycamp.spray.oauth2.server.ScalaRedisTokenService.{ AccessTokenRecord, Settings }
import binarycamp.spray.oauth2.server.TokenService.{ TokenRevocationFailed, TokenRevoked, RevocationResponse, ScopedUserId }
import com.redis.RedisClient
import com.typesafe.config.Config
import scala.concurrent.duration.Duration
import scala.concurrent.{ ExecutionContext, Future }
import spray.util._

final class ScalaRedisTokenService(client: RedisClient, generator: TokenGenerator, settings: Settings)
    extends TokenService {

  import settings._

  require(clientTimeout.isFinite())

  import com.redis.serialization.SprayJsonSupport._
  import spray.json.DefaultJsonProtocol._

  implicit val accessTokenJsonFormat = jsonFormat3(AccessToken.apply(_: String, _: String, _: Option[Map[String, String]]))
  implicit val accessTokenItemJsonFormat = jsonFormat5(AccessTokenRecord)

  implicit val timeout = Timeout(clientTimeout.length, clientTimeout.unit)

  override def issueToken(clientId: String, userId: String, grantType: GrantType, scopes: Set[String], refreshToken: Boolean)(implicit ec: ExecutionContext): Future[Option[TokenResponse]] = {
    val token = generator.accessToken
    val key = toKey(token)

    client.setex(key, keyExpiresIn, AccessTokenRecord(token, clientId, userId, grantType.toString, scopes)).map {
      case _ ⇒ Some(TokenResponse(token, keyExpiresIn, scopes))
    } recover {
      case _ ⇒ None
    }
  }

  override def refreshToken(refreshToken: String, scopes: Option[Set[String]])(implicit ec: ExecutionContext): Future[Option[TokenResponse]] =
    Future.successful(None)

  override def verifyToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[Option[ScopedUserId]] = {
    client.get[AccessTokenRecord](toKey(accessToken)).map {
      case Some(AccessTokenRecord(_, _, userId, _, scopes)) ⇒ Some(ScopedUserId(userId, scopes))
      case _ ⇒ None
    }
  }

  override def revokeToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[RevocationResponse] = {
    client.del(toKey(accessToken)).map {
      case 1    ⇒ TokenRevoked
      case 0    ⇒ TokenRevocationFailed("No such token found!")
      case more ⇒ TokenRevocationFailed(s"$more tokens revoked!")
    }
  }

  private def toKey(token: AccessToken) = s"$keyPrefix:${token.tokenType}:${token.token}"
}

object ScalaRedisTokenService {
  final case class Settings(host: String, port: Int, actorName: String, keyPrefix: String, keyExpiresIn: Int,
                            clientTimeout: Duration)

  object Settings {
    def fromConfig(config: Config): Settings = {
      val c = config.getConfig("scala-redis")
      Settings(
        c.getString("host"),
        c.getInt("port"),
        c.getString("actor-name"),
        c.getString("key-prefix"),
        config.getInt("key-expires-in"),
        c.getDuration("client-timeout"))
    }
  }

  def apply(client: RedisClient, generator: TokenGenerator, settings: Settings): ScalaRedisTokenService =
    new ScalaRedisTokenService(client, generator, settings)

  def default(implicit serverSettings: ServerSettings, refFactory: ActorRefFactory, generator: TokenGenerator): ScalaRedisTokenService = {
    val settings = Settings.fromConfig(serverSettings.tokenService)
    val client = RedisClient(settings.host, settings.port, name(settings.actorName))
    apply(client, generator, settings)
  }

  private def name(base: String) = s"$base-${nameSequence.next()}"
  private val nameSequence = Iterator from 0

  private[server] final case class AccessTokenRecord(accessToken: AccessToken, clientId: String, userId: String, grantType: String, scopes: Set[String])
}
