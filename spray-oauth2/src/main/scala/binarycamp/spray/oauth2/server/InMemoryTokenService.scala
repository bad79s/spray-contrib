package binarycamp.spray.oauth2.server

import binarycamp.spray.oauth2.common._
import binarycamp.spray.oauth2.server.TokenService.{ TokenRevoked, RevocationResponse, ScopedUserId }
import com.typesafe.config.Config
import java.util.UUID
import java.util.concurrent._
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.{ ExecutionContext, Future }

class InMemoryTokenService(config: Config) extends TokenService {
  import binarycamp.spray.oauth2.server.InMemoryTokenService._

  val expiration = config.getLong("key-expires-in")

  private val accessTokens = new ConcurrentHashMap[String, AccessTokenItem]()
  private val accessTokenExpirationQueue = new DelayQueue[DelayedId]()
  private val flushCounter = new AtomicInteger()

  override def issueToken(clientId: String, userId: String, grantType: GrantType, scopes: Set[String], refreshToken: Boolean)(implicit ec: ExecutionContext): Future[Option[TokenResponse]] = {
    if (flushCounter.incrementAndGet() > 100) {
      flushCounter.set(0)
      flush()
    }

    val token = UUID.randomUUID().toString
    val accessToken = BearerToken(token)
    val expiry = System.currentTimeMillis() + expiration * 1000

    accessTokens.put(token, AccessTokenItem(accessToken, clientId, userId, grantType, scopes, expiry))
    accessTokenExpirationQueue.add(DelayedId(expiry, token))
    Future.successful(Some(TokenResponse(accessToken, expiration, scopes)))
  }

  override def refreshToken(refreshToken: String, scopes: Option[Set[String]])(implicit ec: ExecutionContext): Future[Option[TokenResponse]] =
    Future.successful(None)

  override def verifyToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[Option[ScopedUserId]] =
    Future.successful {
      if (accessTokens.containsKey(accessToken.token)) {
        val item = accessTokens.get(accessToken.token)
        if (!item.isExpired && item.accessToken == accessToken) Some(ScopedUserId(item.userId, item.scopes))
        else None
      } else None
    }

  override def revokeToken(accessToken: AccessToken)(implicit ec: ExecutionContext): Future[RevocationResponse] = {
    Future.successful {
      flush()
      TokenRevoked
    }
  }

  private def flush(): Unit = {
    var delayed = accessTokenExpirationQueue.poll()
    while (delayed != null) {
      accessTokens.remove(delayed.id)
      delayed = accessTokenExpirationQueue.poll()
    }
  }

}

object InMemoryTokenService {
  def apply(config: Config): InMemoryTokenService = new InMemoryTokenService(config)

  def default(implicit settings: ServerSettings): InMemoryTokenService = apply(settings.tokenService)

  private[server] case class AccessTokenItem(accessToken: AccessToken, clientId: String, userId: String, grantType: GrantType, scopes: Set[String], expiry: Long) {
    def isExpired: Boolean = System.currentTimeMillis() > expiry
  }

  private[server] case class DelayedId(expiry: Long, id: String) extends Delayed {
    import java.util.concurrent.TimeUnit.MILLISECONDS
    override def getDelay(unit: TimeUnit): Long = math.max(expiry - System.currentTimeMillis(), 0)
    override def compareTo(that: Delayed): Int = getDelay(MILLISECONDS).compareTo(that.getDelay(MILLISECONDS))
  }
}
