package binarycamp.spray.oauth2.server

import binarycamp.spray.common.Error

object Errors {
  sealed abstract class OAuth2Error(override val code: String) extends Error

  private val `invalid_request` = "invalid_request"
  private val `invalid_client` = "invalid_client"
  private val `invalid_scope` = "invalid_scope"
  private val `access_denied` = "access_denied"
  private val `server_error` = "server_error"
  private val `unsupported_grant_type` = "unsupported_grant_type"
  private val `unsupported_response_type` = "unsupported_response_type"

  case object InvalidFormData extends OAuth2Error(`invalid_request`)
  case object FragmentNotAllowed extends OAuth2Error(`invalid_request`)
  case class MissingRequiredParameter(name: String) extends OAuth2Error(`invalid_request`)
  case class DuplicateParameter(duplicates: Set[String]) extends OAuth2Error(`invalid_request`)
  case object ClientCredentialsInReqBody extends OAuth2Error(`invalid_request`)
  case object ClientAuthenticationFailed extends OAuth2Error(`invalid_client`)
  case object ClientMustAuthenticate extends OAuth2Error(`invalid_client`)
  case class UnknownClient(id: String) extends OAuth2Error(`invalid_client`)
  case object UnspecifiedClient extends OAuth2Error(`invalid_client`)
  case object InvalidRedirectEndpoint extends OAuth2Error(`invalid_request`)
  case object EmptyScope extends OAuth2Error(`invalid_scope`)
  case class UnsupportedScope(scopes: Set[String]) extends OAuth2Error(`invalid_scope`)
  case object UserHasDeniedAccess extends OAuth2Error(`access_denied`)
  case object TokenCreationFailed extends OAuth2Error(`server_error`)
  case object UserMustAuthenticate extends OAuth2Error(`server_error`)
  case class UnsupportedGrantType(grantType: Option[String]) extends OAuth2Error(`unsupported_grant_type`)
  case object ServerError extends OAuth2Error(`server_error`)
  case class UnsupportedResponseType(responseType: String) extends OAuth2Error(`unsupported_response_type`)
}
