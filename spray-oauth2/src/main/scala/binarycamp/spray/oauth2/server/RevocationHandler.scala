package binarycamp.spray.oauth2.server

import binarycamp.spray.oauth2.server.TokenService.RevocationResponse
import spray.routing.{ Directive1 }

trait RevocationHandler {
  def revoke: Directive1[RevocationResponse]
}
