package binarycamp.spray.oauth2.server

import binarycamp.spray.pimp._
import com.typesafe.config.Config
import scala.collection.JavaConversions._
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal
import spray.http.Uri
import spray.routing.authentication._
import spray.util.LoggingContext

private[server] class ClientServiceFromConfig(config: Config)(implicit logger: LoggingContext) extends ClientService {
  private val clients = (for (id ← config.root.unwrapped().keys) yield id -> parseClient(id)).toMap

  private def parseClient(id: String): Client =
    try {
      // format: OFF
      val c               = config.getConfig(id)
      val name            = c.getString("name")
      val grantTypes      = c.getStringList("grant-types").map(s => GrantTypes.parse(s)).toSet
      val secret          = c.getString("secret")
      val scopes          = c.getStringSet("scopes")
      val defaultScopes   = c.getOptStringSet("default-scopes")
      val policy          = c.getOptString("scope-policy").map(ScopePolicies.parse).getOrElse(ScopePolicies.FailOnInvalid)
      val redirectionUris = c.getStringList("redirection-uris").map(Uri(_))
      val needsApproval   = c.getBoolean("needs-approval")
      // format: ON

      Client(id, secret, name, grantTypes, scopes, defaultScopes, policy, redirectionUris, needsApproval)
    } catch { case NonFatal(e) ⇒ sys.error(s"Cannot read client $id") }

  def apply(id: String): Future[Option[Client]] = Future.successful(clients.get(id))

  def authenticator(implicit ec: ExecutionContext): ClientAuthenticator = {
    val authenticator: UserPassAuthenticator[Client] = {
      case Some(UserPass(id, secret)) ⇒ Future.successful(clients.get(id).filter(_.secret == secret))
      case None                       ⇒ Future.successful(None)
    }
    BasicAuth(authenticator, "Secured resource")
  }
}