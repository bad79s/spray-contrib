package binarycamp.spray.oauth2.server.flows

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.data.MarshallableError
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.RequestResponseElements._
import binarycamp.spray.oauth2.server.ResponseSender._
import binarycamp.spray.oauth2.server._
import scala.concurrent.ExecutionContext
import shapeless.{ ::, HNil }
import spray.http.Uri
import spray.routing.Directives._
import spray.routing.{ Directive, Directive1, Route }

object ImplicitFlow {
  def handlers(clientService: ClientService, tokenService: TokenService)(implicit ec: ExecutionContext): Seq[RequestHandler] =
    Seq(new ImplicitRequestHandler(clientService, tokenService))
}

class ImplicitRequestHandler[U](clientService: ClientService, tokenService: TokenService)(implicit ec: ExecutionContext)
    extends AuthorizationRequestHandler with ParamMapWriters with RequestHandlerSupport {

  val knownParams = Set(ResponseType, ClientId, RedirectionUri, ScopeParam, State)

  override def canHandle(params: RequestParameters): Boolean = params.get(ResponseType).exists(_ == "token")

  override def validate(params: RequestParameters): Directive1[RequestParameters] =
    provide(params.filter(key ⇒ knownParams.contains(key)))

  override def clientContext(params: RequestParameters): Directive[Client :: Uri :: HNil] =
    clientContext(clientService, params)

  override def handle(userId: String, client: Client, redirectionUri: Uri, scopes: Set[String], params: RequestParameters): Route = {
    if (!client.hasImplicit) return reject(ErrorRejection(ClientMustAuthenticate))
    onSuccess(tokenService.issueToken(client.id, userId, GrantTypes.Implicit, scopes, false)) {
      case Some(token) ⇒ sendInFragment(token, redirectionUri, params.get(State))
      case None        ⇒ reject(ErrorRejection(TokenCreationFailed))
    }
  }

  override def handleError(error: MarshallableError, redirectionUri: Uri, params: RequestParameters): Route =
    sendInFragment(error, redirectionUri, params.get(State))
}
