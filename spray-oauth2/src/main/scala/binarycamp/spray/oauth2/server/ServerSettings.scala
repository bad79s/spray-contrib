package binarycamp.spray.oauth2.server

import akka.actor.ActorRefFactory
import binarycamp.spray.pimp._
import com.typesafe.config.Config
import spray.http.Uri.Path
import spray.util._

case class ServerSettings(
  authorizationEndpoint: Path,
  approvalEndpoint: Path,
  denialEndpoint: Path,
  revocationEndpoint: Path,
  tokenEndpoint: Path,
  scopes: Config,
  clients: Config,
  tokenService: Config)

object ServerSettings extends SettingsCompanion[ServerSettings]("binarycamp.spray.oauth2.server") {
  override def fromSubConfig(c: Config): ServerSettings =
    ServerSettings(
      c.getRelativeUriPath("authorization-endpoint"),
      c.getRelativeUriPath("approval-endpoint"),
      c.getRelativeUriPath("denial-endpoint"),
      c.getRelativeUriPath("revocation-endpoint"),
      c.getRelativeUriPath("token-endpoint"),
      c.getConfig("scopes"),
      c.getConfig("clients"),
      c.getConfig("token-service"))

  implicit def default(implicit refFactory: ActorRefFactory) = apply(actorSystem)
}
